/**
 * Seed the initial data for the app
 */

var types = ['selectable', 'bool', 'temperature', 'number', 'text', 'time'];

var typesSelector = function(current) {
	return {
		'key': 'type',
		'label': 'Type',
		'type': 'selectable',
		'values': types,
		'defaultValue': current,
		'controlled': true
	};
}
var paramSeed = {
	'selectable': {
		'params': [
			{
				'key': 'name',
				'label': 'Name',
				'type': 'text'
			},
			{
				'key': 'key',
				'label': 'Key',
				'type': 'text'
			},
			typesSelector('selectable'),
		]
	},
	'bool': {
		params: [
			typesSelector('bool'),
		]
	},
	'temperature': {
		params: [
			typesSelector('temperature'),
		]
	},
	'number': {
		params: [
			typesSelector('number'),
		]
	},
	'text': {
		params: [
			typesSelector('text'),
		]
	},
	'time': {
		params: [
			typesSelector('time'),
		]
	},
};

var seedMenu = {
	"pizza": {
		'name': 'Pizza',
		'key': 'pizza',
		'params': [
			{
				'key': 'kind',
				'label': 'Kind of Bread',
				'type': 'selectable',
				'values': ['wheat', 'oats', 'mixed', 'sweet-bread', 'vitamin-enriched']
			},
			{
				'key': 'shape',
				'label': 'Shape of pizza',
				'type': 'selectable',
				'values': ['Circular', 'Triangle', 'Square']
			},
			{
				'key': 'sliceCount',
				'label': 'Number of slices',
				'type': 'number',
				'min': 3,
				'max': 12,
				'defaultValue': 3
			},
			{
				'key': 'size',
				'label': 'Size of pizza',
				'type': 'selectable',
				'values': ['small', 'regular', 'family']
			},
			{
				'key': 'ingredients',
				'label': 'Main Ingredients',
				'type': 'selectable',
				'values': ['chicken pieces', 'mutton pieces', 'potato slices', 'tomato slices', 'mushroom slices', 'chillies', 'onion slices'],
				'allow_multiselect': true
			},
			{
				'key': 'temperature',
				'label': 'Temperature of baking',
				'type': 'temperature',
			},
			{
				'key': 'time',
				'label': 'Time to bake',
				'type': 'time',
				'units': 'minutes'
			},
		]
	},
	"burger": {
		'name': 'Burger',
		'key': 'burger',
		'params': [
			{
				'key': 'bread_type',
				'label': 'Bread type',
				'type': 'selectable',
				'values': ['wheat', 'mixed flour', 'oats', 'wheat with spices added']
			},
			{
				'key': 'coating',
				'label': 'Coating',
				'type': 'selectable',
				'values': ['mayonnaise', 'mayonnaise made without egg', 'sauce']
			},
			{
				'key': 'filling',
				'label': 'Filling',
				'type': 'selectable',
				'values': ['potato mash', 'chicken', 'cutlet', 'fish mash']
			},
			{
				'key': 'cabbage_filling',
				'label': 'Add cabbage filling?',
				'type': 'bool',
			},
		]
	},
	"sandwitch": {
		'name': 'Sandwitch',
		'key': 'sandwitch',
		'params': [
			{
				'key': 'should_grill',
				'label': 'Should Grill?',
				'type': 'bool',
			},
			{
				'key': 'time_to_grill',
				'label': 'Time to Grill',
				'type': 'time',
				'units': 'minutes',
				'visible?': function(state) {return state.should_grill === "true";}
			},
			{
				'key': 'time_to_toast',
				'label': 'Time to Toast',
				'type': 'time',
				'units': 'minutes',
				'visible?': function(state) {return state.should_grill === "false";}
			},
			{
				'key': 'filling',
				'label': 'Filling',
				'type': 'selectable',
				'values': ['tuna', 'egg', 'carrot', 'tomato', 'onion']
			},
			{
				'key': 'sliceCount',
				'label': 'Number of slices',
				'type': 'number',
				'min': 1,
				'max': 4,
				'defaultValue': 2
			},
			{
				'key': 'hot_or_cold',
				'label': 'Hot or Cold?',
				'type': 'selectable',
				'values': ['hot', 'cold']
			},
		]
	},
	"icecream": {
		'name': 'Ice Cream',
		'key': 'icecream',
		'params': [
			{
				'key': 'time_to_refrigerate',
				'label': 'Time to Refrigerate',
				'type': 'time',
				'units': 'minutes',
			},
			{
				'key': 'temperature',
				'label': 'Temperature of Serving',
				'type': 'temperature',
				'min': -40,
				'max': 0,
				'defaultValue': -5,
			},
			{
				'key': 'flavor',
				'label': 'Flavor',
				'type': 'selectable',
				'values': ['vanilla', 'strawberry', 'pista', 'mango']
			},
			{
				'key': 'type',
				'label': 'Type',
				'type': 'selectable',
				'values': ['cup', 'cone', 'bar'],
				'valueFilter': {
					'cone': function(state) {return state.flavor !== 'mango';},
					'bar':  function(state) {return state.flavor !== 'strawberry' && state.flavor !== 'pista';}
				}
			},
			{
				'key': 'choco_chips',
				'label': 'Add chocolate chips?',
				'type': 'bool',
			},
			{
				'key': 'topping',
				'label': 'Topping',
				'type': 'selectable',
				'values': ['honey', 'litchi']
			},
			{
				'key': 'sliceCount',
				'label': 'Number of slices',
				'type': 'number',
				'min': 1,
				'max': 4,
				'defaultValue': 1,
				'visible?': function(state) { return state.type === "cup"; }
			},
			{
				'key': 'size',
				'label': 'Size',
				'type': 'selectable',
				'values': ['small', 'regular', 'large'],
				'visible?': function(state) { return state.type === "cone" || state.type === "bar"; }
			},
		]
	},
	"milkshake": {
		'name': 'Milk Shake',
		'key': 'milkshake',
		'params': [
			{
				'key': 'flavor',
				'label': 'Flavor',
				'type': 'selectable',
				'values': ['vanilla', 'strawberry', 'pista']
			},
			{
				'key': 'add_icecream',
				'label': 'Add Icecream?',
				'type': 'bool',
			},
			{
				'key': 'icecream_flavor',
				'label': 'Icecream Flavor',
				'type': 'selectable',
				'values': ['vanilla', 'strawberry', 'pista', 'mango'],
				'visible?': function(state) {return state.add_icecream === 'true'}
			},
			{
				'key': 'temperature',
				'label': 'Temperature of Serving',
				'type': 'temperature',
			},
			{
				'key': 'glass_size',
				'label': 'Glass Size',
				'type': 'selectable',
				'values': ['small', 'large']
			},
			{
				'key': 'milk_brand',
				'label': 'Milk Brand',
				'type': 'selectable',
				'values': ['Amul', 'Aavin', 'DairyFresh']
			},
		]
	},
	'meals': {
		'name': 'Indian Meals',
		'key': 'meals',
		'params': [
			{
				'key': 'rice_type',
				'label': 'Rice Type',
				'type': 'selectable',
				'values': ['Ponni', 'basmati', 'Raw rice']
			},
			{
				'key': 'sambar_type',
				'label': 'Sambar Type',
				'type': 'selectable',
				'values': ['plain', 'onion', 'mango']
			},
			{
				'key': 'variety_rices',
				'label': 'Variety rices to add',
				'type': 'selectable',
				'values': ['lemon', 'brinji', 'tomato'],
				'allow_multiselect': true
			},
			{
				'key': 'add_curd',
				'label': 'Add Curd?',
				'type': 'bool',
			},
			{
				'key': 'veg_curries',
				'label': 'Veg curries to add?',
				'type': 'veg_curry',
				'multi': true,
				'min': 2,
				'max': 5
			},
		]
	},
	'veg_curry': {
		'name': 'Veg Curry',
		'key': 'veg_curry',
		'params': [
			{
				'key': 'vegetable',
				'label': 'Vegetables',
				'type': 'selectable',
				'values': ['brinjal', 'potato', 'cauliflower', 'ladies finger']
			},
			{
				'key': 'spices',
				'label': 'Spices To Add',
				'type': 'selectable',
				'values': ['white pepper', 'black pepper', 'cumin']
			},
			{
				'key': 'fried_or_boiled',
				'label': 'Fried or Boiled?',
				'type': 'selectable',
				'values': ['Fried', 'Boiled']
			},
		]
	}
};

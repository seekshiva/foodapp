
var Panel = ReactBootstrap.Panel,
	Row = ReactBootstrap.Row,
	Col = ReactBootstrap.Col,
	Input = ReactBootstrap.Input,
	Glyphicon = ReactBootstrap.Glyphicon,
	ListGroup = ReactBootstrap.ListGroup,
	ListGroupItem = ReactBootstrap.ListGroupItem;


var Store = React.createClass({
	getInitialState: function() {
		return {
			"selectedFood": null
		};
	},
	_closeDialog: function(e) {
		e.preventDefault();
		this.setState({"selectedFood": null});
	},
	_selectFood: function(key, e, reactId) {
		this.setState({"selectedFood": key});
	},
	editSelectedFood: function() {
		if(this.state.selectedFood === null) return null;

		var food = this.props.menu[this.state.selectedFood];
		var customFood = food.params
						.filter(function(p) { return types.indexOf(p.type) === -1; })
						.map(function(p)    { return this.props.menu[p.type]; }, this)
						.reduce(function(obj, k) {
							obj[k.key] = k; return obj;
						}, {});
		return <AddItem food={food} customFood={customFood} closeDialog={this._closeDialog} />;
	},
	render: function() {
		var menuObj = this.props.menu,
			menu = [];

		for(itemName in menuObj) {
			var item = menuObj[itemName];
			var args = {
				onClick: this._selectFood.bind(null, itemName),
				key: itemName
			};
			if(itemName === this.state.selectedFood)
				args['className'] = 'active';

			menu.push((
				<ListGroupItem {...args}>
					<Glyphicon glyph='chevron-right' className='pull-right' />
					{item['name']}
				</ListGroupItem>
			));
		}
		return (
		    <div id="app" className="container">
				<Row fill>
					<Col xs={5}>
						<Panel header="Menu">
							<ListGroup fill className='clickable'>{menu}</ListGroup>
						</Panel>
					</Col>
					<Col xs={7}>{this.editSelectedFood()}</Col>
				</Row>
			</div>
		);
	}
});

var AddItem = React.createClass({
	_onChangeCallback: function(key, e) {},
	render: function() {
		var Panel = ReactBootstrap.Panel;
		var title = (
			<h3>
				<a onClick={this.props.closeDialog} className="pull-right clickable" >
					<Glyphicon glyph="remove" />
				</a>
				{this.props.food.name}
			</h3>
		);
		return (
			<Panel header={title}>
				<Form component={this.props.food} customTypes={this.props.customFood} />
			</Panel>
		);
	}
});

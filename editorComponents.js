var EditMode = React.createClass({
	render: function() {
		return (
			<div>
				<Editor menu={this.props.menu} />
				<hr />
				<BeautifulJson json={this.props.menu} />
			</div>
		);
	}
});

var Editor = React.createClass({
	getInitialState: function() {
		return {
			'selectedFood': null,
			'selectedParam': null,
			'menu': {}
		};
	},
	_selectFood: function(key) {
		if(this.state.selectedFood !== null && this.state.selectedFood.key === key)
			return;
		this.setState({'selectedFood': this.state.menu[key], 'selectedParam': null});
	},
	_editParam: function(key) {
		var selectedParam = this.state.selectedFood.params.filter(function(param) {
			return param.key == key
		})[0];
		console.log(key);
		this.setState({'selectedParam': selectedParam});
	},
	componentWillMount: function() {
		//TODO: should I use setState() here?
		this.setState({menu: this.props.menu});
	},
	render: function() {
		var listOfComponents = [];
		for(var key in this.state.menu) {
			var item = this.state.menu[key],
				args = {
					key: item.key,
					className: 'clickable',
					onClick: this._selectFood.bind(null, item.key)
				};
			listOfComponents.push(
			    <ListGroupItem {...args}>{item.name}</ListGroupItem>
			);
		}

		var editParam = this.state.selectedFood == null ? null : (
				<Col xs={4}>
					<EditParam param={this.state.selectedParam} />
				</Col>
			);

		return (
			<Row>
				<Col xs={3}>
					<ListGroup fill className='clickable'>{listOfComponents}</ListGroup>
				</Col>
				<Col xs={4}>
					<SelectFood food={this.state.selectedFood} _editParam={this._editParam} />
				</Col>
				{editParam}
			</Row>
		);
	}
});

var SelectFood = React.createClass({
	render: function() {
		var food = this.props.food;
		if(food === null) {
			return <div>Select food type</div>;
		}

		var paramList = food.params.map(function(param, index) {
			var args = {
				key: param.key,
				onClick: this.props._editParam.bind(null, param.key)
			}
			return (
					<ListGroupItem {...args}>
						<strong>{param.label} </strong><br />
						<em>{param.type}</em>
					</ListGroupItem>
				);
		}, this);

		return (
			<div>
				<Input label='name' type='text' value={food.name} />
				<Input label='key' type='text' value={food.key} />
				<Panel header='params'>
					<ListGroup fill className='clickable'>{paramList}</ListGroup>
				</Panel>
			</div>
		);
	}
});

var EditParam = React.createClass({
	getInitialState: function() {
		return {
			param: this.props.param
		};
	},
	render: function() {
		var param = this.props.param;
		if(param === null || param === undefined) return <div>select a param</div>;

		return (
				<div>
					<h3>{param.key}</h3>
					<Form component={paramSeed[param.type]} />
				</div>
			);
	}
});

var Form = React.createClass({
	getInitialState: function() {
		var params = this.props.component.params;
		return this._getStateFromParams(params);
	},
	componentWillReceiveProps: function(nextProps) {
		var params = nextProps.component.params;
		this.setState(this._getStateFromParams(params));
	},
	_getStateFromParams: function(params) {
		var state = {};

		for(var i=0; i< params.length; ++i) {
			var param = params[i];
			if('defaultValue' in param) {
				state[param.key] = param.defaultValue;
			} else {
				//TODO: use min/max value for number, temperature, time if present

				state[param.key] = (function() {
					switch(param.type) {
						case 'selectable':
							if('allow_multiselect' in param && param.allow_multiselect === true) {
								return [];
							} else {
								return param.values[0];
							}
							break;
						case 'number':
						case 'temperature':
						case 'time':
							return 0; break;
						case 'bool':
							return "false"; break;
					};
					return null;
				})();
			}
		}
		return state;
	},
	_onChangeCallback: function(key, e) {
		var delta = {};
		delta[key] = e.target.value;
		//TODO: do validation before set state
		this.setState(delta);
	},
	render: function() {
		var fields = [],
			params = this.props.component.params,
			rootNode = this.props.rootNode ? this.props.rootNode : 'form';

		for(var i=0; i< params.length; ++i) {
			var param = params[i], input = null, children = null;

			if('visible?' in param && param['visible?'](this.state) === false) {
				continue;
			}

			var input = (function(state, _onChangeCallback, customTypes) {
				var props = {
					id: param.key,
					onChange: _onChangeCallback.bind(null, param.key),
					defaultValue: state[param.key],
					label: param.label,
					// value: state[param.key],
				};
				if(param.type === 'time') {
					props['min'] = 0;
					props['addonAfter'] = param.units;
				}
				if('min' in param)	props['min'] = param.min;
				if('max' in param)	props['max'] = param.max;

				if(param.controlled === true)
					props['value'] = state[param.key];

				switch(param.type) {
					case 'selectable':
						if('allow_multiselect' in param) props['multiple'] = param.allow_multiselect;
						var valueFilter = 'valueFilter' in param ? param.valueFilter : {};

						var options =	param.values.filter(function(option) {
											if(option in valueFilter) {
												return valueFilter[option](state);
											}
											return true;
										}).map(function(option, index) {
											return <option key={index}>{option}</option>;
										});
						return <Input type='select' {...props}>{options}</Input>;
					case 'text':
						return <Input type="text" {...props} />;
					case 'time':
					case 'number':
					case 'temperature':
						return <Input type="number" {...props} />;
					case 'bool':
						return (
								<Input type='select' id={param.key} {...props}>
									<option value="true">True</option>
									<option value="false">False</option>
								</Input>
							);
						break;
				}
				// couldn't recognize type.. checking if it is one of the menu items
				if(param.type in customTypes) {
					var args = {
						component: customTypes[param.type]
					};
					if(param.multi) {
						if(param.min) args['min'] = param.min;
						if(param.max) args['max'] = param.max;
						return <MultiForm {...args} />;
					} else {
						return <Form {...args} rootNode="div" />;
					}
				}
				return (
					<em>Unrecognized type: {param.type}</em>
				);
			})(this.state, this._onChangeCallback, this.props.customTypes);


			fields.push(input);
		}

		if(rootNode === 'form') {
			return <form>{fields}</form>;
		} else {
			return <div>{fields}</div>;
		}
	}
});

var MultiForm = React.createClass({
	getInitialState: function() {
		return {
			'deleteAllowed': true,
			'count': 1,
			'min': 0,
			'max': 100
		};
	},
	_decrCounter: function(e) {
		e.preventDefault();
		this.setState({count: this.state.count - 1});
		if(this.state.count <= this.props.min + 1)
			this.setState({deleteAllowed: false});
	},
	_incrCounter: function(e) {
		e.preventDefault();
		this.setState({count: this.state.count + 1});
		if(this.state.count <= this.props.min)
			this.setState({deleteAllowed: true});
	},
	componentWillMount: function() {
		if(this.props.min)
			this.setState({
				'min': this.props.min,
				'count': this.props.min
			});
		if(this.props.max)
			this.setState({
				'max': this.props.max
			});
	},
	render: function() {
		var forms = [],
			decrCounter = this.state.count <= this.state.min ? null : <button onClick={this._decrCounter}>-</button>,
			incrCounter = this.state.count >= this.state.max ? null : <button onClick={this._incrCounter}>+</button>;

		for(i=0; i<this.state.count; ++i) {
			forms.push((
				<ListGroupItem key={i}>
					<Form component={this.props.component} rootNode="div" />
				</ListGroupItem>
			));
		}
		return (
			<ListGroup fill>
				{forms}
				<ListGroupItem>
					{decrCounter}
					{incrCounter}
				</ListGroupItem>
			</ListGroup>
		);
	}
});

var BeautifulJson = React.createClass({
	render: function() {
		return (
			<pre style={{height: 600, overflow: 'scroll'}}>
				{JSON.stringify(this.props.json, null, "\t")}
			</pre>
		);
	}
});
